package data

type AdminDataRequest struct {
	TypeRequest string `bson:"typeRequest" json:"typerequest"`
	Collection  string `bson:"collection" json:"collection"`
}
type Pymsg struct {
	Id  int `bson:"id" json:"id"`
	Cat string `bson:"cat" json:"cat"`
}
