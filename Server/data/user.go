package data

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Utente struct {
	Codice    primitive.ObjectID `bson:"_id" json:"codice"`
	Email     string             `bson:"email" json:"email"`
	Nome      string             `bson:"nome" json:"nome"`
	Password  string             `bson:"password" json:"password"`
	Exp       int                `bson:"exp" json:"exp"`
	LevelUser LevelUser          `bson:"leveluser" json:"leveluser"`
	Crediti   uint32             `bson:"crediti" json:"crediti"`
}
type UtenteRegister struct {
	Email    string `bson:"email" json:"email"`
	Nome     string `bson:"nome" json:"nome"`
	Password string `bson:"password" json:"password"`
	Exp      uint   `bson:"exp" json:"exp"`
	Crediti  uint32 `bson:"crediti" json:"crediti"`
}

type LevelUser struct {
	Level int `bson:"level" json:"level"`
	Exp   int `bson:"exp" json:"exp"`
}

type CategoryBuy struct {
	BuyerCode    primitive.ObjectID `bson:"buyer" json:"buyer"`
	CategoryCode primitive.ObjectID `bson:"category" json:"category"`
}
