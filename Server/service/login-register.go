package service

import (
	"Server/data"
	"Server/utils"
	"encoding/json"
	"log"
	"net"
	"strconv"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

const (
	superadmin_token string = "Az8jyTErX5g48kbAp/LypWlLvzZXpWzBPVXXNQ7Rm2Q="
	MAX                     = 1000
)

func Register(u_json string, conn net.Conn, mongodb *mongo.Database) {
	//D is an ordered representation of a BSON documen
	var result bson.D
	var msg string
	user := data.UtenteRegister{}
	//  json to user convert
	json.Unmarshal([]byte(u_json), &user)
	//setto il punteggio 0 di un utente appena registrato
	user.Exp = 0
	user.Crediti = 0
	log.Println("utente: ", u_json, " user: ", user)
	/*
		Prima dell'inserimento
		verifica che l'utente non sia già registrato
	*/
	filter := bson.D{{Key: "email", Value: user.Email}}
	err := utils.FindOne(filter, "utenti", mongodb).Decode(&result)
	/*
		Se l'errore è nil vuol dire che l'email inserita è presente nel database, in caso contrario
		passiamo al controllo del nome utente
	*/
	if err != nil {
		filter = bson.D{{Key: "nome", Value: user.Nome}}
		err = utils.FindOne(filter, "utenti", mongodb).Decode(&result)
	}
	/*
		Se l'errore è nil vuol dire che l'utente è gia registrato
		altrimenti si può proceder all'inserimento
	*/
	if err != nil {
		encodedPsw := utils.Encoding(user.Password, user.Email)
		// aggiorno la password con il token
		user.Password = encodedPsw
		err = utils.InsertOne("utenti", mongodb, user)
		if err != nil {
			log.Println("Errore inserimento")
		}
		msg = "Registrazione effettuata"
	} else {
		msg = "Utente già registrato"
	}
	utils.Send([]byte(msg), conn)
}

func Login(out chan string, list []string, u_json string, conn net.Conn, mongodb *mongo.Database) {
	var userData data.Utente
	usLogin := data.Utente{}
	// Conversione da json a Utente
	/*
		Cerchiamo prima se è presente l'utente nel nostro database
	*/
	json.Unmarshal([]byte(u_json), &usLogin)
	//verifico se è stata ricevuto una mail o un username
	isMail := (len(usLogin.Email) > 0)
	var filter primitive.D
	if isMail {
		usLogin.Password = utils.Encoding(usLogin.Password, usLogin.Email)
		filter = bson.D{{Key: "email", Value: usLogin.Email}, {Key: "password", Value: usLogin.Password}}
	} else {
		var userS data.Utente
		filterName := bson.D{{Key: "nome", Value: usLogin.Nome}}
		utils.FindOne(filterName, "utenti", mongodb).Decode(&userS)
		usLogin.Password = utils.Encoding(usLogin.Password, userS.Email)
		filter = bson.D{{Key: "nome", Value: usLogin.Nome}, {Key: "password", Value: usLogin.Password}}
	}
	err := utils.FindOne(filter, "utenti", mongodb).Decode(&userData)
	if err != nil {
		msg := "Errore utente non trovato"
		utils.Send([]byte(msg), conn)
		out <- ""
	} else {
		if cercaUtente(usLogin.Password, list) {
			msg := "Errore Utente già loggato"
			utils.Send([]byte(msg), conn)
			out <-""
			return
		}
		checkAdmin := (usLogin.Password == superadmin_token)
		admin := strconv.FormatBool(checkAdmin)
		if checkAdmin {
			utils.Send([]byte(admin), conn)
			out <- usLogin.Password
		} else {
			utils.Send([]byte(admin), conn)
			lvl := CalcLevel(userData.Exp)
			userData.LevelUser = lvl
			json_comp, _ := json.Marshal(userData)
			utils.Send(json_comp, conn)
			out <- usLogin.Password
		}
	}
}

func cercaUtente(Token string, users []string) bool {
	for i := 0; i < MAX; i++ {
		if users[i] == Token {
			return true
		}
	}
	return false
}
