package utils

import (
	"crypto/sha256"
	"encoding/base64"
	"log"
)

func Encoding(str string, str2 string) string {

	sha := sha256.New()
	/* il token è generato dall'unione dell'email
	e della password
	*/
	sha.Write([]byte(str + str2))
	hashStr := string(sha.Sum(nil))
	// converto la stringa in base 64
	token := base64.StdEncoding.EncodeToString([]byte(hashStr))
	log.Println("token: " + token)
	return token
}
