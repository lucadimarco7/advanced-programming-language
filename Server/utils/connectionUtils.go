package utils

import (
	"Server/data"
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"math"
	"net"
	"os"
	"strconv"
)

const (
	hostname string = "127.0.0.1"
	port     string = "8000"
)

func CheckError(err error) bool {
	if err != nil {
		fmt.Fprintf(os.Stderr, "Fatal error: %s", err.Error())
		return true
	}
	return false
}

func SendToModule(ujson string) []byte {
	msg := data.Pymsg{}
	json.Unmarshal([]byte(ujson), &msg)
	addrs, err := net.LookupHost(hostname)
	res2 := CheckError(err)
	if res2 {
		msg := "statistiche non disponibili"
		bytemsg := []byte(msg)
		return bytemsg
	}
	fmt.Println("Server address: ", addrs, ":", port)
	// service: address:8000
	service := addrs[0] + ":" + port
	tcpAddr, err := net.ResolveTCPAddr("tcp4", service)
	res2 = CheckError(err)
	if res2 {
		msg := "statistiche non disponibili"
		bytemsg := []byte(msg)
		return bytemsg
	}
	conn, err := net.DialTCP("tcp", nil, tcpAddr)
	res2 = CheckError(err)
	if res2 {
		msg := "statistiche non disponibili"
		bytemsg := []byte(msg)
		return bytemsg
	}
	json_comp, _ := json.Marshal(msg)
	conn.Write([]byte(json_comp))
	res := Receive(conn)
	return res
}

func Send(msg []byte, conn net.Conn) {
	/*
		Quando viene mandato un messaggio i primi 16 byte
		contengono la dimensione del messaggio
	*/
	bytemsg := []byte(msg)
	len_msg := strconv.Itoa(len(bytemsg))
	byteslen_msg := []byte(len_msg)
	byteslen := make([]byte, 16)
	diff := len(byteslen) - len(byteslen_msg)
	for i := 0; i < len(byteslen_msg); i++ {
		byteslen[diff+i-1] = byteslen_msg[i]
	}
	// L'ultimo byte contiene un ritorno a capo
	byteslen[len(byteslen)-1] = 10 //10 = ritorno a capo
	log.Println("Send: ", byteslen)
	conn.Write(byteslen)
	conn.Write(bytemsg)
}

func Receive(conn net.Conn) []byte {
	/*
		In ricezione prima arriva la lunghezza del messaggio
		16 byte di cui l'ultimo carattere è un ritorno a capo
	*/
	byteslen := make([]byte, 16)
	conn.Read(byteslen)
	var len_msg int
	log.Println(byteslen)
	for i := 0; i < len(byteslen); i++ {
		exp := len(byteslen) - i - 2
		if byteslen[i] > 47 && byteslen[i] < 58 {
			value := int(byteslen[i]) - 48
			len_msg += value * int(math.Pow(10, float64(exp)))
		}
	}
	bytesmsg := make([]byte, len_msg)
	for len(bytes.Trim(bytesmsg, "\x00")) != len_msg {
		conn.Read(bytesmsg)
	}
	if len(bytesmsg) < 512 {
		log.Println("Receive message: ", string(bytesmsg))
	}
	message := string(bytesmsg)
	log.Println("Receive message: ", message)

	return bytesmsg
}
