﻿using Newtonsoft.Json;


namespace Prova.Data
{
    class Category
    {
        [JsonProperty("_id")]
        public string? Codice { get; init; }

        [JsonProperty("name")]
        public string? Name { get; init; }

        [JsonProperty("price")]
        public int Price { get; init; }
    }
}
