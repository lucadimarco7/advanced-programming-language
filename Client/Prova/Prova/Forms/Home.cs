﻿using Prova.Connection;

namespace Prova.Forms
{
    public partial class Home : Form
    {
        private readonly Login parent;
        private readonly Protocol pt;

        public Home(Login startingForm)
        {
            InitializeComponent();
            parent = startingForm;
            pt = new Protocol();
        }

        private void BtnGame_Click(object sender, EventArgs e)
        {
            Game frm = new(this);
            ShowForm(frm);
        }

        private void BtnProfile_Click(object sender, EventArgs e)
        {
            Profile frm = new(this);
            ShowForm(frm);
        }

        private void BtnShop_Click(object sender, EventArgs e)
        {
            Shop frm = new(this);
            ShowForm(frm);
        }

        private void BtnRankings_Click(object sender, EventArgs e)
        {
            Rankings frm = new(this);
            ShowForm(frm);
        }

        private void ShowForm(Form frm)
        {
            frm.Show();
            this.Visible = false;
        }

        private void BtnLogOut_Click(object sender, EventArgs e)
        {
            pt.SetProtocolID("logout");
            SocketTCP.Send(pt.ToString());
            parent.Visible = true;
            this.Close();
        }

        protected override void OnClosed(EventArgs e)
        {
            if (!parent.Visible)
            {
                pt.SetProtocolID("close");
                SocketTCP.Send(pt.ToString());
                Application.Exit();
            }
        }

        private void btnOnline_Click(object sender, EventArgs e)
        {
            Online frm = new(this);
            ShowForm(frm);
        }
    }
}
