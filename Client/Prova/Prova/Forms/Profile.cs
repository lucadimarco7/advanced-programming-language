﻿using Microsoft.VisualBasic;
using Prova.Utils;
using Prova.Connection;
using Prova.Data;
using System.Diagnostics;

namespace Prova.Forms
{
    public partial class Profile : Form
    {
        private readonly Home parent;
        private readonly Protocol pt;

        public Profile(Home startingForm)
        {
            InitializeComponent();
            parent = startingForm;
            pt = new Protocol();
        }

        private void Profile_VisibleChanged(object sender, EventArgs e)
        {
            if (Singleton.Instance.User != null &&
                Singleton.Instance.User.LevelUser != null)
            {
                txtUsername.Text = Singleton.Instance.User.Nome;
                txtEmail.Text = Singleton.Instance.User.Email;
                txtLevel.Text = Singleton.Instance.User.LevelUser!.Level.ToString();
                txtExp.Text = Singleton.Instance.User.LevelUser!.Exp.ToString();
                txtCredits.Text = Singleton.Instance.User.Crediti.ToString();
            }
            else
            {
                Debug.Print("Singleton istance user is null");
            }
        }

        private void BtnEditUsername_Click(object sender, EventArgs e)
        {
            string input = Interaction.InputBox("Inserisci password:", "Modifica", "");
            string input2 = Interaction.InputBox("Inserisci nuovo nome utente", "Modifica", "");

            if (CheckFields.CheckUsername(input2))
            {
                string profileJson = System.Text.Json.JsonSerializer.Serialize(new
                {
                    PasswordCorrente = input,
                    CampoModificato = "nome",
                    NuovoValore = input2
                });

                pt.SetProtocolID("editProfile");
                pt.Data = profileJson;
                SocketTCP.Send(pt.ToString());
                string responseData = SocketTCP.Receive();

                if (responseData.Contains("false"))
                {
                    MessageBox.Show("Modifica fallita, ricontrolla la password.", "Errore",
                                MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    Singleton.Instance.User.Nome = input2;
                    MessageBox.Show("Nome Utente modificato con successo.", "Completato",
                                MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Profile_VisibleChanged(this, e);
                }
            }
            else
            {
                MessageBox.Show("Nome Utente non valido.\n" +
                    "Puoi usare A-z, 0-9. La lunghezza minima è 5 caratteri.", "Errore",
                                MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void BtnEditEmail_Click(object sender, EventArgs e)
        {
            string input = Interaction.InputBox("Inserisci password:", "Modifica", "");
            string input2 = Interaction.InputBox("Inserisci nuova E-Mail", "Modifica", "");
            string check = CheckFields.CheckEmail(input2);

            if (check.Equals("E-Mail valida"))
            {
                string profileJson = System.Text.Json.JsonSerializer.Serialize(new
                {
                    PasswordCorrente = input,
                    CampoModificato = "email",
                    NuovoValore = input2
                });

                pt.SetProtocolID("editProfile");
                pt.Data = profileJson;
                SocketTCP.Send(pt.ToString());
                string responseData = SocketTCP.Receive();

                if (responseData.Contains("false"))
                {
                    MessageBox.Show("Modifica fallita, ricontrolla la password.", "Errore",
                                MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    Singleton.Instance.User.Email = input2;
                    MessageBox.Show("E-Mail modificata con successo.", "Completato",
                                MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Profile_VisibleChanged(this, e);
                }
            }
            else
            {
                MessageBox.Show(check, "Errore",
                                MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void BtnEditPassword_Click(object sender, EventArgs e)
        {
            string input = Interaction.InputBox("Inserisci vecchia password:", "Modifica", "");
            string input2 = Interaction.InputBox("Inserisci nuova password:", "Modifica", "");
            string input3 = Interaction.InputBox("Conferma nuova password:", "Modifica", "");
            string check = CheckFields.CheckPassword(input2, input3);

            if (check.Equals("Password valida"))
            {
                Debug.WriteLine("Username modificato, nuovo username: " + input);
                string profileJson = System.Text.Json.JsonSerializer.Serialize(new
                {
                    PasswordCorrente = input,
                    CampoModificato = "password",
                    NuovoValore = input2,
                });

                pt.SetProtocolID("editProfile");
                pt.Data = profileJson;

                SocketTCP.Send(pt.ToString());
                string responseData = SocketTCP.Receive();
                Debug.WriteLine("PASSWORD: " + responseData);
                if (responseData.Contains("false"))
                {
                    MessageBox.Show("Modifica fallita, ricontrolla la vecchia password", "Errore",
                                MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    MessageBox.Show("Password modificata con successo.", "Completato",
                                MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            else
            {
                MessageBox.Show(check, "Errore",
                                MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        protected override void OnClosed(EventArgs e)
        {
            parent.Visible = true;
        }
    }
}
