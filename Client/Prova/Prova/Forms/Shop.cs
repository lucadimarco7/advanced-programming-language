﻿using Newtonsoft.Json;
using Prova.Connection;
using Prova.Data;
using Prova.UserControls;
using System.Diagnostics;

namespace Prova.Forms
{
    public partial class Shop : Form
    {
        private readonly Home parent;
        private readonly Protocol pt;

        public Shop(Home startingForm)
        {
            InitializeComponent();
            parent = startingForm;
            pt = new Protocol();
        }

        private void Shop_Load(object sender, EventArgs e)
        {
            lblCrediti.Text = Singleton.Instance.User.Crediti.ToString();
            flowLayoutPanel1.Enabled = true;
            Category[]? categories;

            string categoryJson = System.Text.Json.JsonSerializer.Serialize(
                    new
                    {
                        Request = "store",
                    }
                    );
            pt.SetProtocolID("questions");
            pt.Data = categoryJson;
            SocketTCP.Send(pt.ToString());
            string response = SocketTCP.Receive();
            categories = JsonConvert.DeserializeObject<Category[]>(response);

            if (categories != null)
                PopulateItemsCategories(categories, categories.Length);
            else
                Debug.Print("Categories are null");
        }

        private void PopulateItemsCategories(Category[] c, int index)
        {
            flowLayoutPanel1.Controls.Clear();
            List<UCShopCategoryModel> listCategories = new();

            for (int i = 0; i < index; i++)
            {
                if (c[i].Name != null)
                {
                    listCategories.Add(new UCShopCategoryModel());
                    listCategories[i].SetName(c[i].Name!, c[i].Price);
                    listCategories[i].UCShopCategoryButtonClicked += new EventHandler(BtnCategory_Click);
                    flowLayoutPanel1.Controls.Add(listCategories[i]);
                }
                else
                {
                    Debug.Print("Category's name is null");
                }
            }
        }

        private void BtnCategory_Click(object? sender, EventArgs e)
        {
            flowLayoutPanel1.Enabled = false;
            if (sender != null)
            {
                int price = Int16.Parse(((string)sender).Split('\n')[1]);

                if (Singleton.Instance.User.Crediti >= price)
                {
                    pt.SetProtocolID("buyCategory");
                    string categoryJson = System.Text.Json.JsonSerializer.Serialize(
                            new
                            {
                                Name = ((string)sender).Split('\n')[0],
                                Price = price
                            }
                            );
                    pt.Data = categoryJson;
                    SocketTCP.Send(pt.ToString());
                    string response = SocketTCP.Receive();

                    if (response.Contains("Errore"))
                    {
                        MessageBox.Show(response, "Errore",
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        MessageBox.Show(response, "Acquisto Completato",
                            MessageBoxButtons.OK, MessageBoxIcon.Information);
                        string userJson = System.Text.Json.JsonSerializer.Serialize(new
                        {
                            request = "one"
                        });
                        pt.SetProtocolID("users");
                        pt.Data = userJson;
                        SocketTCP.Send(pt.ToString());
                        response = SocketTCP.Receive();
                        User? user = JsonConvert.DeserializeObject<User>(response);
                        if (user != null)
                            Singleton.Instance.User.Crediti = user.Crediti;                 
                    }
                }
                else
                {
                    MessageBox.Show("Crediti non sufficienti!", "Errore",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            else
            {
                Debug.Print("Sender is null");
            }

            Shop_Load(this, e);
        }

        protected override void OnClosed(EventArgs e)
        {
            parent.Visible = true;
        }
    }
}
