﻿using Newtonsoft.Json;
using Prova.Connection;
using Prova.Data;
using System.Diagnostics;

namespace Prova.Forms
{
    public partial class Rankings : Form
    {
        private readonly Home parent;
        private readonly Protocol pt;
        private Category[]? categories;
        private string ranking = String.Empty;

        public Rankings(Home startingForm)
        {
            InitializeComponent();
            parent = startingForm;
            pt = new Protocol();
        }

        private void Stats_Load(object sender, EventArgs e)
        {
            string categoryJson = System.Text.Json.JsonSerializer.Serialize(
                    new
                    {
                        Request = "update",
                    }
                    );
            pt.SetProtocolID("questions");
            pt.Data = categoryJson;
            SocketTCP.Send(pt.ToString());
            string response = SocketTCP.Receive();
            categories = JsonConvert.DeserializeObject<Category[]>(response);

            tabControl.TabPages.Clear();
            if (categories != null)
            {
                foreach (Category c in categories)
                    tabControl.TabPages.Add(c.Name);
            }
            else
            {
                Debug.Print("Categories are null");
            }
            TabControl_SelectedIndexChanged(this, e);
        }

        private void TabControl_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tabControl.TabCount > 0 && categories != null)
            {
                tabControl.Enabled = false;
                Cursor.Current = Cursors.WaitCursor;

                if (categories[0].Name != null)
                {
                    string category = categories[0].Name!;

                    if (tabControl.SelectedTab != null)
                        category = tabControl.SelectedTab.Text;
                    else
                        tabControl.SelectTab(0);

                    string json = System.Text.Json.JsonSerializer.Serialize(
                            new
                            {
                                id = 4,
                                cat = category
                            }
                            );
                    pt.SetProtocolID("stats");
                    pt.Data = json;
                    SocketTCP.Send(pt.ToString());
                    ranking = SocketTCP.Receive();
                    if (ranking == "statistiche non disponibili") {
                        MessageBox.Show("statistiche non disponibili, riprovare più tardi", "Errore",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                    if (tabControl.SelectedTab != null)
                    {
                        tabControl.SelectedTab.BackgroundImage = Utils.ImageUtils.Base64ToImage(ranking);
                        tabControl.SelectedTab.BackgroundImageLayout = ImageLayout.Zoom;
                    }
                    else
                    {
                        Debug.Print("No tab selected");
                    }

                    tabControl.Enabled = true;
                    Cursor.Current = Cursors.Default;
                }
                else
                {
                    Debug.Print("Category's name is null");
                }
            }
            else
            {
                Debug.Print("Categories are null");
            }
        }

        protected override void OnClosed(EventArgs e)
        {
            parent.Visible = true;
        }
    }
}
