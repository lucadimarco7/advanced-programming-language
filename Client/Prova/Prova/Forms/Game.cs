﻿using Prova.Data;
using Prova.UserControls.Game;
using System.Diagnostics;

namespace Prova.Forms
{
    public partial class Game : Form
    {
        private readonly Home parent;
        private string game = string.Empty;

        public Game(Home startingForm)
        {
            InitializeComponent();
            parent = startingForm;
        }

        private void Btn_Click(object sender, EventArgs e)
        {
            if (((Button)sender).Tag != null)
            {
                game = ((Button)sender).Tag.ToString()!;
            }
            else
            {
                Debug.Print("Tag is null");
            }
            this.Controls.Clear();
            if (game != null)
            {
                UCGameCategories ucGameCategories = new(game);
                this.Controls.Add(ucGameCategories);
                ucGameCategories.UCGameButtonClicked += new EventHandler(ShowGame);
            }
            else
            {
                Debug.Print("Game is null");
            }
        }

        private void ShowGame(object? sender, EventArgs e)
        {
            this.Controls.Clear();
            if (sender != null)
            {
                List<Question> list = ((UCGameCategories)sender).List;
                string category = ((UCGameCategories)sender).Category;
                if ((!game.Equals("im") && list.Count > 30) || (game.Equals("im") && list.Count > 0))
                {
                    switch (game)
                    {
                        case "cl":
                            this.Controls.Add(new UCClassicGame(list, category));
                            break;

                        case "tm":
                            this.Controls.Add(new UCTimeGame(list, category));
                            break;

                        case "tf":
                            this.Controls.Add(new UCTrueFalseGame(list, category));
                            break;

                        case "im":
                            this.Controls.Add(new UCHangmanGame(list, category));
                            break;
                    }
                }
                else
                {
                    MessageBox.Show("Categoria al momento non disponibile", "Impossibile avviare la partita",
                            MessageBoxButtons.OK, MessageBoxIcon.Information);
                    InitializeComponent();
                }
            }
            else
            {
                Debug.Print("Sender is null");
            }
        }

        private void Game_HelpButtonClicked(object sender, System.ComponentModel.CancelEventArgs e)
        {
            MessageBox.Show("Seleziona una modalità:\n" +
                "Classico: 30 domande con 4 opzioni di risposta. Se sbagli il gioco finisce.\n" +
                "A tempo: 1 minuto di gioco e 30 domande, realizza la serie più lunga e i tuoi record di risposte esatte.\n" +
                "Vero o Falso: 30 domande con vero o falso come opzioni di risposta. Se sbagli il gioco finisce.\n" +
                "Impiccato: Hai 5 vite per trovare la risposta alla domanda indovinando le sue lettere. Ogni tentativo errato toglie una vita.\n\n" +
                "Seleziona una categoria:\n" +
                "Scegli di quale ambito faranno parte le domande durante il gioco.\n\n" +
                "N.B.: I record valgono solo nella modalità a tempo.", "Regole", MessageBoxButtons.OK);
        }

        protected override void OnClosed(EventArgs e)
        {
            if (this.Controls[0] is Prova.UserControls.Game.UCTimeGame)
            {
                UCTimeGame uc = (UCTimeGame)this.Controls[0];
                uc.StopTimer();
            }
            this.Controls.Clear();
            parent.Visible = true;
        }
    }
}
