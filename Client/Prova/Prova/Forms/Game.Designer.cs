﻿namespace Prova.Forms
{
    partial class Game
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnHangman = new System.Windows.Forms.Button();
            this.btnTimeGame = new System.Windows.Forms.Button();
            this.btnTrueFalseGame = new System.Windows.Forms.Button();
            this.btnClassic = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnHangman
            // 
            this.btnHangman.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnHangman.Location = new System.Drawing.Point(195, 149);
            this.btnHangman.Name = "btnHangman";
            this.btnHangman.Size = new System.Drawing.Size(135, 70);
            this.btnHangman.TabIndex = 3;
            this.btnHangman.Tag = "im";
            this.btnHangman.Text = "Impiccato";
            this.btnHangman.UseVisualStyleBackColor = true;
            this.btnHangman.Click += new System.EventHandler(this.Btn_Click);
            // 
            // btnTimeGame
            // 
            this.btnTimeGame.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnTimeGame.Location = new System.Drawing.Point(195, 49);
            this.btnTimeGame.Name = "btnTimeGame";
            this.btnTimeGame.Size = new System.Drawing.Size(135, 70);
            this.btnTimeGame.TabIndex = 1;
            this.btnTimeGame.Tag = "tm";
            this.btnTimeGame.Text = "A Tempo";
            this.btnTimeGame.UseVisualStyleBackColor = true;
            this.btnTimeGame.Click += new System.EventHandler(this.Btn_Click);
            // 
            // btnTrueFalseGame
            // 
            this.btnTrueFalseGame.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnTrueFalseGame.Location = new System.Drawing.Point(12, 149);
            this.btnTrueFalseGame.Name = "btnTrueFalseGame";
            this.btnTrueFalseGame.Size = new System.Drawing.Size(135, 70);
            this.btnTrueFalseGame.TabIndex = 2;
            this.btnTrueFalseGame.Tag = "tf";
            this.btnTrueFalseGame.Text = "Vero o Falso";
            this.btnTrueFalseGame.UseVisualStyleBackColor = true;
            this.btnTrueFalseGame.Click += new System.EventHandler(this.Btn_Click);
            // 
            // btnClassic
            // 
            this.btnClassic.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnClassic.Location = new System.Drawing.Point(12, 49);
            this.btnClassic.Name = "btnClassic";
            this.btnClassic.Size = new System.Drawing.Size(135, 70);
            this.btnClassic.TabIndex = 0;
            this.btnClassic.Tag = "cl";
            this.btnClassic.Text = "Classico";
            this.btnClassic.UseVisualStyleBackColor = true;
            this.btnClassic.Click += new System.EventHandler(this.Btn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(186, 19);
            this.label1.TabIndex = 0;
            this.label1.Text = "Seleziona una modalità";
            // 
            // Game
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(344, 231);
            this.Controls.Add(this.btnTimeGame);
            this.Controls.Add(this.btnTrueFalseGame);
            this.Controls.Add(this.btnHangman);
            this.Controls.Add(this.btnClassic);
            this.Controls.Add(this.label1);
            this.HelpButton = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(360, 270);
            this.Name = "Game";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Game";
            this.HelpButtonClicked += new System.ComponentModel.CancelEventHandler(this.Game_HelpButtonClicked);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private Button btnClassic;
        private Button btnHangman;
        private Button btnTrueFalseGame;
        private Button btnTimeGame;
        private Label label1;
    }
}