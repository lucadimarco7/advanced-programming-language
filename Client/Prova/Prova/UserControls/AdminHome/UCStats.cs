﻿using Newtonsoft.Json;
using Prova.Connection;
using Prova.Data;
using System.Diagnostics;

namespace Prova.UserControls.AdminHome
{
    public partial class UCStats : UserControl
    {
        private readonly Protocol pt;
        private Category[]? categories;
        private string stats = String.Empty;

        public UCStats()
        {
            InitializeComponent();
            pt = new Protocol();
        }

        private void UCStats_Load(object sender, EventArgs e)
        {
            this.Dock = DockStyle.Fill;

            string categoryJson = System.Text.Json.JsonSerializer.Serialize(
                    new
                    {
                        Request = "update",
                    }
                    );
            pt.SetProtocolID("questions");
            pt.Data = categoryJson;
            SocketTCP.Send(pt.ToString());
            string response = SocketTCP.Receive();

            categories = JsonConvert.DeserializeObject<Category[]>(response);

            cbCategory.Items.Add("Nessun Filtro");
            if (categories != null)
            {
                foreach (Category c in categories)
                    cbCategory.Items.Add(c.Name);
            }
            else
            {
                Debug.Print("Categories are null");
            }
        }

        private void ComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            cbStat.Enabled = false;
            cbCategory.Enabled = false;
            Cursor.Current = Cursors.WaitCursor;

            string category = String.Empty;

            if (cbCategory.SelectedIndex > -1 && !cbCategory.SelectedItem.ToString()!.Equals("Nessun Filtro"))
                category = cbCategory.SelectedItem.ToString()!;

            if (cbStat.SelectedIndex > -1)
            {
                string json = System.Text.Json.JsonSerializer.Serialize(
                        new
                        {
                            id = cbStat.SelectedIndex,
                            cat = category
                        }
                        );
                pt.SetProtocolID("stats");
                pt.Data = json;
                SocketTCP.Send(pt.ToString());
                stats = SocketTCP.Receive();

                panel1.BackgroundImage = Utils.ImageUtils.Base64ToImage(stats);
                panel1.BackgroundImageLayout = ImageLayout.Zoom;

            }

            cbStat.Enabled = true;
            if(cbStat.SelectedIndex != 3)
                cbCategory.Enabled = true;
            Cursor.Current = Cursors.Default;
        }
    }
}
