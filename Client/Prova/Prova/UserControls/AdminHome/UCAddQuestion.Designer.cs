﻿namespace Prova.UserControls.AdminHome
{
    partial class UCAddQuestion
    {
        /// <summary> 
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione componenti

        /// <summary> 
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare 
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnConfirm = new System.Windows.Forms.Button();
            this.txtCategory = new System.Windows.Forms.TextBox();
            this.lblCategory = new System.Windows.Forms.Label();
            this.txtCorrectOption = new System.Windows.Forms.TextBox();
            this.txtOption3 = new System.Windows.Forms.TextBox();
            this.txtOption2 = new System.Windows.Forms.TextBox();
            this.txtOption1 = new System.Windows.Forms.TextBox();
            this.lblCorrectOption = new System.Windows.Forms.Label();
            this.lblOption3 = new System.Windows.Forms.Label();
            this.lblOption2 = new System.Windows.Forms.Label();
            this.lblOption1 = new System.Windows.Forms.Label();
            this.txtQuestion = new System.Windows.Forms.TextBox();
            this.lblQuestion = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // btnConfirm
            // 
            this.btnConfirm.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnConfirm.Location = new System.Drawing.Point(748, 292);
            this.btnConfirm.Name = "btnConfirm";
            this.btnConfirm.Size = new System.Drawing.Size(73, 23);
            this.btnConfirm.TabIndex = 6;
            this.btnConfirm.Text = "Conferma";
            this.btnConfirm.UseVisualStyleBackColor = true;
            this.btnConfirm.Click += new System.EventHandler(this.BtnConfirm_Click);
            // 
            // txtCategory
            // 
            this.txtCategory.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCategory.Location = new System.Drawing.Point(655, 167);
            this.txtCategory.Multiline = true;
            this.txtCategory.Name = "txtCategory";
            this.txtCategory.Size = new System.Drawing.Size(166, 23);
            this.txtCategory.TabIndex = 5;
            this.txtCategory.TextChanged += new System.EventHandler(this.TxtCategory_TextChanged);
            // 
            // lblCategory
            // 
            this.lblCategory.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCategory.AutoSize = true;
            this.lblCategory.Location = new System.Drawing.Point(655, 149);
            this.lblCategory.Name = "lblCategory";
            this.lblCategory.Size = new System.Drawing.Size(142, 15);
            this.lblCategory.TabIndex = 23;
            this.lblCategory.Text = "Crea una nuova categoria";
            // 
            // txtCorrectOption
            // 
            this.txtCorrectOption.Location = new System.Drawing.Point(275, 259);
            this.txtCorrectOption.Multiline = true;
            this.txtCorrectOption.Name = "txtCorrectOption";
            this.txtCorrectOption.Size = new System.Drawing.Size(195, 55);
            this.txtCorrectOption.TabIndex = 4;
            // 
            // txtOption3
            // 
            this.txtOption3.Location = new System.Drawing.Point(73, 259);
            this.txtOption3.Multiline = true;
            this.txtOption3.Name = "txtOption3";
            this.txtOption3.Size = new System.Drawing.Size(195, 55);
            this.txtOption3.TabIndex = 3;
            // 
            // txtOption2
            // 
            this.txtOption2.Location = new System.Drawing.Point(275, 167);
            this.txtOption2.Multiline = true;
            this.txtOption2.Name = "txtOption2";
            this.txtOption2.Size = new System.Drawing.Size(195, 55);
            this.txtOption2.TabIndex = 2;
            // 
            // txtOption1
            // 
            this.txtOption1.Location = new System.Drawing.Point(73, 168);
            this.txtOption1.Multiline = true;
            this.txtOption1.Name = "txtOption1";
            this.txtOption1.Size = new System.Drawing.Size(195, 55);
            this.txtOption1.TabIndex = 1;
            // 
            // lblCorrectOption
            // 
            this.lblCorrectOption.AutoSize = true;
            this.lblCorrectOption.Location = new System.Drawing.Point(275, 241);
            this.lblCorrectOption.Name = "lblCorrectOption";
            this.lblCorrectOption.Size = new System.Drawing.Size(97, 15);
            this.lblCorrectOption.TabIndex = 18;
            this.lblCorrectOption.Text = "Opzione Corretta";
            // 
            // lblOption3
            // 
            this.lblOption3.AutoSize = true;
            this.lblOption3.Location = new System.Drawing.Point(73, 241);
            this.lblOption3.Name = "lblOption3";
            this.lblOption3.Size = new System.Drawing.Size(60, 15);
            this.lblOption3.TabIndex = 17;
            this.lblOption3.Text = "Opzione 3";
            // 
            // lblOption2
            // 
            this.lblOption2.AutoSize = true;
            this.lblOption2.Location = new System.Drawing.Point(275, 150);
            this.lblOption2.Name = "lblOption2";
            this.lblOption2.Size = new System.Drawing.Size(60, 15);
            this.lblOption2.TabIndex = 16;
            this.lblOption2.Text = "Opzione 2";
            // 
            // lblOption1
            // 
            this.lblOption1.AutoSize = true;
            this.lblOption1.Location = new System.Drawing.Point(73, 150);
            this.lblOption1.Name = "lblOption1";
            this.lblOption1.Size = new System.Drawing.Size(60, 15);
            this.lblOption1.TabIndex = 15;
            this.lblOption1.Text = "Opzione 1";
            // 
            // txtQuestion
            // 
            this.txtQuestion.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtQuestion.Location = new System.Drawing.Point(73, 61);
            this.txtQuestion.Multiline = true;
            this.txtQuestion.Name = "txtQuestion";
            this.txtQuestion.Size = new System.Drawing.Size(748, 66);
            this.txtQuestion.TabIndex = 0;
            // 
            // lblQuestion
            // 
            this.lblQuestion.AutoSize = true;
            this.lblQuestion.Location = new System.Drawing.Point(73, 43);
            this.lblQuestion.Name = "lblQuestion";
            this.lblQuestion.Size = new System.Drawing.Size(34, 15);
            this.lblQuestion.TabIndex = 13;
            this.lblQuestion.Text = "Testo";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(483, 150);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(131, 15);
            this.label1.TabIndex = 24;
            this.label1.Text = "Seleziona una categoria";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(483, 167);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(166, 23);
            this.comboBox1.TabIndex = 25;
            // 
            // UCAddQuestion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtCategory);
            this.Controls.Add(this.lblCategory);
            this.Controls.Add(this.txtCorrectOption);
            this.Controls.Add(this.txtOption3);
            this.Controls.Add(this.txtOption2);
            this.Controls.Add(this.txtOption1);
            this.Controls.Add(this.lblCorrectOption);
            this.Controls.Add(this.lblOption3);
            this.Controls.Add(this.lblOption2);
            this.Controls.Add(this.lblOption1);
            this.Controls.Add(this.txtQuestion);
            this.Controls.Add(this.lblQuestion);
            this.Controls.Add(this.btnConfirm);
            this.Name = "UCAddQuestion";
            this.Size = new System.Drawing.Size(900, 400);
            this.Load += new System.EventHandler(this.UCAddQuestion_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private Button btnConfirm;
        private TextBox txtCategory;
        private Label lblCategory;
        private TextBox txtCorrectOption;
        private TextBox txtOption3;
        private TextBox txtOption2;
        private TextBox txtOption1;
        private Label lblCorrectOption;
        private Label lblOption3;
        private Label lblOption2;
        private Label lblOption1;
        private TextBox txtQuestion;
        private Label lblQuestion;
        private Label label1;
        private ComboBox comboBox1;
    }
}
