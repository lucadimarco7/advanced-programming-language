﻿using Newtonsoft.Json;
using Prova.Connection;
using Prova.Data;
using System.Diagnostics;

namespace Prova.UserControls.AdminHome
{
    public partial class UCEditQuestions : UserControl
    {
        private readonly Protocol pt;
        private string category = String.Empty;
        private Question[]? questions;

        public UCEditQuestions()
        {
            InitializeComponent();
            pt = new Protocol();
        }

        private void UCEditQuestions_Load(object sender, EventArgs e)
        {
            this.Dock = DockStyle.Fill;

            string categoryJson = System.Text.Json.JsonSerializer.Serialize(
                    new
                    {
                        Request = "update",
                    }
                    );
            pt.SetProtocolID("questions");
            pt.Data = categoryJson;
            SocketTCP.Send(pt.ToString());
            string response = SocketTCP.Receive();

            Category[]? categories = JsonConvert.DeserializeObject<Category[]>(response);

            cbCategory.Items.Add("Nessun Filtro");
            if (categories != null)
            {
                foreach (Category c in categories)
                    cbCategory.Items.Add(c.Name);
            }
            else
            {
                Debug.Print("Categories are null");
            }
        }

        private void CbCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (cbCategory.SelectedIndex > -1 && !cbCategory.SelectedItem.ToString()!.Equals("Nessun Filtro"))
            if (cbCategory.SelectedIndex > 0)
            {
                category = cbCategory.SelectedItem.ToString()!;
                txtCategory.Enabled = false;
            }
            else
            {
                txtCategory.Enabled = true;
            }
        }

        private void BtnSearch_Click(object sender, EventArgs e)
        {
            List<Question> list = new();
            if (txtCategory.Enabled)
            {
                category = txtCategory.Text;
            }
            string searchJson = System.Text.Json.JsonSerializer.Serialize(
                    new
                    {
                        Testo = txtText.Text,
                        Categoria = category
                    }
                    );
            pt.SetProtocolID("searchQuestions");
            pt.Data = searchJson;
            SocketTCP.Send(pt.ToString());
            string response = SocketTCP.Receive();
            questions = JsonConvert.DeserializeObject<Question[]>(response);

            if (questions != null)
            {
                for (int i = 0; i < questions.Length; i++)
                {
                    list.Add(new Question()
                    {
                        Codice = questions[i].Codice,
                        Categoria = questions[i].Categoria,
                        Testo = questions[i].Testo,
                        Risposta = questions[i].Risposta,
                        Risposta2 = questions[i].Risposta2,
                        Risposta3 = questions[i].Risposta3,
                        RispostaCorretta = questions[i].RispostaCorretta
                    });
                }
            }
            else
            {
                Debug.Print("Questions are null");
            }

            dataGridView.DataSource = questions;
            dataGridView.Columns[0].ReadOnly = true;
            dataGridView.Columns[1].ReadOnly = true;
        }

        private void DataGridView_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            int colIndex = dataGridView.CurrentCell.ColumnIndex;

            string str = dataGridView.Columns[colIndex].HeaderText;
            str = str.Length == 1 ? char.ToLower(str[0]).ToString() : char.ToLower(str[0]) + str[1..];

            string editQuestionJson = System.Text.Json.JsonSerializer.Serialize(
                    new
                    {
                        Codice = dataGridView.CurrentRow.Cells[0].Value,
                        CampoModificato = str,
                        NuovoValore = dataGridView.CurrentCell.Value
                    }
                    );
            pt.SetProtocolID("editQuestion");
            pt.Data = editQuestionJson;
            SocketTCP.Send(pt.ToString());
            string response = SocketTCP.Receive();

            MessageBox.Show(response, "Conferma", MessageBoxButtons.OK, MessageBoxIcon.Information);
            if (!response.Equals("Success"))
                BtnSearch_Click(this, e);
        }

        private void BtnDelete_Click(object sender, EventArgs e)
        {
            string deleteQuestionJson = System.Text.Json.JsonSerializer.Serialize(
                    new
                    {
                        Codice = dataGridView.CurrentRow.Cells[0].Value,
                    }
                    );
            pt.SetProtocolID("deleteQuestion");
            pt.Data = deleteQuestionJson;
            SocketTCP.Send(pt.ToString());
            string response = SocketTCP.Receive();

            MessageBox.Show(response, "Conferma", MessageBoxButtons.OK, MessageBoxIcon.Information);
            BtnSearch_Click(this, e);
        }
    }
}
