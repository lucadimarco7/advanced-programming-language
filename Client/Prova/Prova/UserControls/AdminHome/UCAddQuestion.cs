﻿using Newtonsoft.Json;
using Prova.Connection;
using Prova.Data;
using System.Diagnostics;

namespace Prova.UserControls.AdminHome
{
    public partial class UCAddQuestion : UserControl
    {
        private readonly Protocol pt;
        private Category[]? categories;
        public event EventHandler? UCAddQuestionClicked;

        public UCAddQuestion()
        {
            InitializeComponent();
            pt = new Protocol();
        }

        private void UCAddQuestion_Load(object sender, EventArgs e)
        {
            this.Dock = DockStyle.Fill;

            string categoryJson = System.Text.Json.JsonSerializer.Serialize(
                    new
                    {
                        Request = "update",
                    }
                    );
            pt.SetProtocolID("questions");
            pt.Data = categoryJson;
            SocketTCP.Send(pt.ToString());
            string response = SocketTCP.Receive();
            categories = JsonConvert.DeserializeObject<Category[]>(response);

            if (categories != null)
            {
                foreach (Category c in categories)
                    comboBox1.Items.Add(c.Name);
            }
            else
            {
                Debug.Print("Categories are null");
            }
        }
        private void TxtCategory_TextChanged(object sender, EventArgs e)
        {
            if (txtCategory.Text.Equals(""))
                comboBox1.Enabled = true;
            else
                comboBox1.Enabled = false;
        }

        private void BtnConfirm_Click(object sender, EventArgs e)
        {
            string questions = string.Empty;
            string category;

            if (comboBox1.Enabled && comboBox1.SelectedItem != null)
                category = comboBox1.SelectedItem.ToString()!;
            else
                category = txtCategory.Text;
            string questionJson = System.Text.Json.JsonSerializer.Serialize(
                new
                {
                    categoria = category,
                    risposta = txtOption1.Text,
                    risposta2 = txtOption2.Text,
                    risposta3 = txtOption3.Text,
                    rispostaCorretta = txtCorrectOption.Text,
                    testo = txtQuestion.Text
                }
                );
            questions += questionJson;
            pt.SetProtocolID("addQuestions");
            pt.Data = questions;
            SocketTCP.Send(pt.ToString());
            string response = SocketTCP.Receive();
            MessageBox.Show(response, "Conferma", MessageBoxButtons.OK, MessageBoxIcon.Information);

            if (response.Equals("La domanda è stata inserita con successo") || response.Contains("La domanda e la categoria"))
            {
                UCAddQuestionClicked?.Invoke(this, e);

            }
        }
    }
}
