﻿using Newtonsoft.Json;
using Prova.Connection;
using Prova.Data;
using System.Diagnostics;
using JsonSerializer = System.Text.Json.JsonSerializer;

namespace Prova.UserControls.Game
{
    public partial class UCGameCategories : UserControl
    {
        private readonly Protocol pt;
        private List<Question> list = new();
        private string category = String.Empty;
        private readonly string game = String.Empty;
        public event EventHandler? UCGameButtonClicked;

        public UCGameCategories(string game)
        {
            InitializeComponent();
            pt = new Protocol();
            this.game = game;
        }

        public List<Question> List{ get => list; }
        public string Category{ get => category; }

        private void UCGameCategories_Load(object sender, EventArgs e)
        {
            Category[]? categories;

            string categoryJson = System.Text.Json.JsonSerializer.Serialize(
                    new
                    {
                        Request = "game",
                    }
                    );
            pt.SetProtocolID("questions");
            pt.Data = categoryJson;
            SocketTCP.Send(pt.ToString());
            string response = SocketTCP.Receive();

            categories = JsonConvert.DeserializeObject<Category[]>(response);
            if (categories != null)
                PopulateItemsCategories(categories, categories.Length);
            else
                Debug.Print("Categories are null");
        }
        private void PopulateItemsCategories(Category[] c, int index)
        {
            flowLayoutPanel1.Controls.Clear();

            List<UCGameCategoriesModel> listCategories = new();

            for (int i = 0; i < index; i++)
            {
                if (c[i].Name != null)
                {
                    listCategories.Add(new UCGameCategoriesModel());
                    listCategories[i].SetName(c[i].Name!);
                    listCategories[i].UCGameCategoryButtonClicked += new EventHandler(BtnCategory_Click);
                    flowLayoutPanel1.Controls.Add(listCategories[i]);
                }
                else
                {
                    Debug.Print("Category's name is null");
                }
            }
        }

        private void BtnCategory_Click(object? sender, EventArgs e)
        {
            if (sender != null)
            {
                category = (string)sender;
                pt.SetProtocolID("game");
                GetQuestions(pt, ((string)sender));
            }
            else
            {
                Debug.Print("Sender is null");
            }
        }

        private void GetQuestions(Protocol pt, string text)
        {
            list = new List<Question>();

            Question[]? questions;

            string questionsJson = JsonSerializer.Serialize(
                new
                {
                    Game = game,
                    Categoria = text
                }
            );
            pt.Data = questionsJson;
            SocketTCP.Send(pt.ToString());
            string response = SocketTCP.Receive();
            questions = JsonConvert.DeserializeObject<Question[]>(response);

            if (questions != null)
            {
                for (int i = 0; i < questions.Length; i++)
                {
                    list.Add(new Question()
                    {
                        Testo = questions[i].Testo,
                        Risposta = questions[i].Risposta,
                        Risposta2 = questions[i].Risposta2,
                        Risposta3 = questions[i].Risposta3,
                        RispostaCorretta = questions[i].RispostaCorretta
                    });
                }
                UCGameButtonClicked?.Invoke(this, new EventArgs());
            }
            else
            {
                Debug.Print("Questions are null");
            }
        }
    }
}