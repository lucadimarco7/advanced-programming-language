﻿namespace Prova.UserControls.Game
{
    partial class UCClassicGame
    {
        /// <summary> 
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione componenti

        /// <summary> 
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare 
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.helpDoublePoints = new System.Windows.Forms.Button();
            this.helpChangeQuestion = new System.Windows.Forms.Button();
            this.helpSolution = new System.Windows.Forms.Button();
            this.help5050 = new System.Windows.Forms.Button();
            this.lblScore = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BackColor = System.Drawing.Color.Transparent;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(12, 9);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(330, 240);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // helpDoublePoints
            // 
            this.helpDoublePoints.Location = new System.Drawing.Point(360, 219);
            this.helpDoublePoints.Name = "helpDoublePoints";
            this.helpDoublePoints.Size = new System.Drawing.Size(113, 23);
            this.helpDoublePoints.TabIndex = 5;
            this.helpDoublePoints.Text = "Doppi Punti";
            this.helpDoublePoints.UseVisualStyleBackColor = true;
            this.helpDoublePoints.Click += new System.EventHandler(this.HelpDoublePoints_Click);
            // 
            // helpChangeQuestion
            // 
            this.helpChangeQuestion.Location = new System.Drawing.Point(360, 190);
            this.helpChangeQuestion.Name = "helpChangeQuestion";
            this.helpChangeQuestion.Size = new System.Drawing.Size(113, 23);
            this.helpChangeQuestion.TabIndex = 4;
            this.helpChangeQuestion.Text = "Cambia Domanda";
            this.helpChangeQuestion.UseVisualStyleBackColor = true;
            this.helpChangeQuestion.Click += new System.EventHandler(this.HelpChangeQuestion_Click);
            // 
            // helpSolution
            // 
            this.helpSolution.Location = new System.Drawing.Point(360, 161);
            this.helpSolution.Name = "helpSolution";
            this.helpSolution.Size = new System.Drawing.Size(113, 23);
            this.helpSolution.TabIndex = 3;
            this.helpSolution.Text = "Soluzione";
            this.helpSolution.UseVisualStyleBackColor = true;
            this.helpSolution.Click += new System.EventHandler(this.HelpSolution_Click);
            // 
            // help5050
            // 
            this.help5050.Location = new System.Drawing.Point(360, 132);
            this.help5050.Name = "help5050";
            this.help5050.Size = new System.Drawing.Size(113, 23);
            this.help5050.TabIndex = 2;
            this.help5050.Text = "50:50";
            this.help5050.UseVisualStyleBackColor = true;
            this.help5050.Click += new System.EventHandler(this.Help5050_Click);
            // 
            // lblScore
            // 
            this.lblScore.AutoSize = true;
            this.lblScore.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lblScore.ForeColor = System.Drawing.Color.Black;
            this.lblScore.Location = new System.Drawing.Point(426, 13);
            this.lblScore.Name = "lblScore";
            this.lblScore.Size = new System.Drawing.Size(18, 19);
            this.lblScore.TabIndex = 1;
            this.lblScore.Text = "0";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(360, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 19);
            this.label1.TabIndex = 0;
            this.label1.Text = "Score:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label2.Location = new System.Drawing.Point(360, 111);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(39, 18);
            this.label2.TabIndex = 5;
            this.label2.Text = "Aiuti";
            // 
            // UCClassicGame
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Controls.Add(this.label2);
            this.Controls.Add(this.helpDoublePoints);
            this.Controls.Add(this.helpChangeQuestion);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.helpSolution);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblScore);
            this.Controls.Add(this.help5050);
            this.Name = "UCClassicGame";
            this.Size = new System.Drawing.Size(484, 261);
            this.Load += new System.EventHandler(this.UCClassicGame_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private FlowLayoutPanel flowLayoutPanel1;
        private Label lblScore;
        private Label label1;
        private Button help5050;
        private Button helpSolution;
        private Button helpChangeQuestion;
        private Button helpDoublePoints;
        private Label label2;
    }
}
