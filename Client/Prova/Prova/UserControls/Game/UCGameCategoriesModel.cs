﻿namespace Prova.UserControls.Game
{
    public partial class UCGameCategoriesModel : UserControl
    {
        public event EventHandler? UCGameCategoryButtonClicked;

        public UCGameCategoriesModel()
        {
            InitializeComponent();
        }

        public void SetName(string name)
        {
            button.Text = name;
        }

        private void Button_Click(object sender, EventArgs e)
        {
            UCGameCategoryButtonClicked?.Invoke(button.Text, e);
        }
    }
}
