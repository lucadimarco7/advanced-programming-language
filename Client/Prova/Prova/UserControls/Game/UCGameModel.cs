﻿namespace Prova.UserControls
{
    public partial class UCGameModel : UserControl
    {
        public event EventHandler? UCGameButtonClicked;
        public bool IsCorrectAnswer { get; set; }

        public UCGameModel()
        {
            InitializeComponent();
        }

        public void SetFields(string question, string option1, string option2, string option3, string option4)
        {
            txtQuestion.Text = question;
            btnOption1.Text = option1;
            btnOption2.Text = option2;
            btnOption3.Text = option3;
            btnOption4.Text = option4;
        }

        private string? correctOption;

        public string SetCorrectOption { set { correctOption = value; } }
        public string GetCorrectOption()
        {
            if (this.correctOption != null)
                return this.correctOption;
            else
                return "Correct Option is null";
        }
        public Button GetOption1 { get => btnOption1; }
        public Button GetOption2 { get => btnOption2; }
        public Button GetOption3 { get => btnOption3; }
        public Button GetOption4 { get => btnOption4; }

        public void BtnOption_Click(object sender, EventArgs e)
        {
            Button b = (Button)sender;
            if (correctOption != null && b.Text.Equals(correctOption))
            {
                IsCorrectAnswer = true;
                b.BackColor = Color.Green;
            }
            else
            {
                IsCorrectAnswer = false;
                b.BackColor = Color.Red;
            }

            UCGameButtonClicked?.Invoke(this, e);
        }
    }
}
