﻿namespace Prova.UserControls.Game
{
    public partial class UCEndGame : UserControl
    {
        public event EventHandler? UCEndGameButtonClicked;

        public UCEndGame()
        {
            InitializeComponent();
        }

        private void BtnHome_Click(object sender, EventArgs e)
        {
            UCEndGameButtonClicked?.Invoke(this, e);
        }

        public void Setlabel(string value)
        {
            label3.Text = value;
        }
    }
}
