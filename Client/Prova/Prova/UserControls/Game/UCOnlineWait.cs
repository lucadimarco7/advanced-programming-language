﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Prova.UserControls.Game
{
    public partial class UCOnlineWait : UserControl
    {
        public UCOnlineWait()
        {
            InitializeComponent();
            Debug.WriteLine("ONLINE WAIT");
        }

        public void SetLabel1(string s)
        {
            label1.Text = s;
        }
    }
}
