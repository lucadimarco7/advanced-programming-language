from stats import *
import socket
import json
import base64

HOSTNAME = socket.gethostbyname('localhost')
PORT_NUMBER = 8000
SIZE = 1024


def send(socket, message):
    """
    Calcolo la lunghezza del messaggio codificata in ascii
    concatenata ad un ritorno a capo ed effettuo il padding
    per avere una lunghezza fissa del messaggio di 16 byte
    """
    lenmsg = (str(len(message)) + "\n").encode(encoding="ascii")
    len_msg = bytearray(16)
    diff = len(len_msg) - len(lenmsg)
    for i in range(len(lenmsg)):
        len_msg[diff + i] = lenmsg[i]
    socket.send(len_msg)
    socket.send(message)


def recv(socket_rcv):
    byte_message = socket_rcv.recv(SIZE)
    msg = byte_message.decode("utf-8")
    return msg


def switch(y1):
    s = Stats()
    if y1['id'] == 0:
        img = s.game_stats("played", y1['cat'])
        return img
    elif y1['id'] == 1:
        img = s.game_stats('scores', y1['cat'])
        return img
    elif y1['id'] == 2:
        img = s.game_stats('scores_avg', y1['cat'])
        return img
    elif y1['id'] == 3:
        img = s.most_purchased_categories()
        return img
    elif y1['id'] == 4:
        img = s.ranking(y1['cat'])
        return img


def main():
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.bind((HOSTNAME, PORT_NUMBER))
    print(f"Server address: {HOSTNAME}")
    print(f"Server listening on port {PORT_NUMBER}")
    server_socket.listen(5)
    flag = True
    while flag:
        new_socket, address = server_socket.accept()
        message = recv(new_socket)
        y = json.loads(message)
        print(f"Received packet from {address}: {y}")
        reply = switch(y)
        with open(reply, 'rb') as img:
            img_byte = base64.b64encode(img.read())
            send(new_socket, img_byte)
        print(f"Sent packet to {address}: {'reply'}")
        new_socket.close()


if __name__ == "__main__":
    main()
