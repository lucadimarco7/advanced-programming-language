from connection import connect
import matplotlib.pyplot as plt
from matplotlib.pyplot import figure
from matplotlib import colors
from matplotlib.ticker import PercentFormatter
import numpy as np
import pandas as pd


class Graph:
    name = ""

    def get_fig(self):
        im = self.name
        return im


class Histogram(Graph):
    figure(figsize=(10, 10), dpi=70)

    def __init__(self):
        pass

    def __init__(self, names, values, ylabel, xlabel):
        self.names = names
        self.values = values
        self.ylabel = ylabel
        self.xlabel = xlabel

    def save_fig(self):
        plt.bar(self.names, self.values)
        plt.ylabel(self.ylabel)
        plt.xlabel(self.xlabel)
        self.name = 'histogram.jpeg'
        plt.savefig(self.name)
        plt.close()


class Table(Graph):
    def __init__(self, entries):
        self.entries = entries

    def save_fig(self):
        n = 10
        fig, ax = plt.subplots(figsize=(10, 2 + n / 2.5))
        # hide axes
        fig.patch.set_visible(False)
        ax.axis('off')
        ax.axis('tight')

        df = pd.DataFrame(self.entries, columns=['Categoria', 'Utente', 'Punti'])

        rcolors = np.full(10, 'linen')
        ccolors = np.full(3, 'lavender')

        table = ax.table(cellText=df.values, colLabels=df.columns, loc='center',
                         cellLoc='center', rowLabels=[1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
                         rowColours=rcolors, rowLoc='center', colColours=ccolors)
        table.scale(1, 2)
        table.set_fontsize(16)
        fig.tight_layout()
        self.name = 'table.jpeg'
        plt.savefig(self.name)
        plt.close()
