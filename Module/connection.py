from pymongo import MongoClient
from bson.objectid import ObjectId

URL = "mongodb://localhost:27017/"
DATABASE = "apl_database"


class connect():
    def __init__(self):
        self.client = MongoClient(URL)
        self.db = self.client[DATABASE]

    def get_id_by_name(self, collection, name):
        filter = {
            'name': name
        }
        project = {
            '_id': 1
        }

        return self.db[collection].find(
            filter=filter,
            projection=project
        )

    def get_collection_id(self, collection):
        return self.db[collection].distinct('_id')

    def get_field_by_id(self, collection, _id, field):
        filter = {
            '_id': ObjectId(_id)
        }
        project = {
            field: 1,
            '_id': 0
        }

        return self.db[collection].find(
            filter=filter,
            projection=project
        )

    def get_records_by_category(self, _id):
        filter = {}
        if _id != '':
            filter = {
                'categorycode': _id
            }
        return self.db['records'].find(filter)

    def get_payments_by_category(self, _id):
        return self.db['Payments'].find({'category': _id})

    def get_categories_played(self, gamecode):
        filter = {}
        if gamecode != '':
            filter = {
                'gamecode': gamecode
            }
        project = {
            'categorycode': 1,
            '_id': 0,
            'totalgame': 1,
            'totalscore': 1
        }
        sort = list({
                        'totalgame': -1
                    }.items())
        return self.db['stats'].find(
            filter=filter,
            projection=project,
            sort=sort
        )

    def get_games_stats(self, categorycode):
        filter = {}
        if categorycode != '':
            filter = {
                'categorycode': categorycode
            }
        project = {
            'gamecode': 1,
            '_id': 0,
            'totalgame': 1,
            'totalscore': 1
        }
        return self.db['stats'].find(
            filter=filter,
            projection=project
        )