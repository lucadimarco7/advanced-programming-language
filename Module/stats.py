from graph import *


class Stats:
    conn = connect()

    def most_purchased_categories(self):
        category_list = {}
        categories = self.conn.get_collection_id("categories")
        payments_count = []
        categories_names = []
        for c in categories:
            category_list[c] = self.conn.get_payments_by_category(c)
            payments_count.append(len(list(category_list[c])))
        for c in category_list:
            name = self.conn.get_field_by_id("categories", c, "name")
            categories_names.append(name[0]['name'])
        h = Histogram(categories_names, payments_count, 'Numero di vendite', 'Categorie')
        h.save_fig()
        return h.get_fig()

    def ranking(self, category_name):
        records_values = []
        if category_name != '':
            categorycode = self.conn.get_id_by_name("categories", category_name)
            records = self.conn.get_records_by_category(categorycode[0]['_id']).limit(10)
            records.sort('score', -1)
        else:
            records = self.conn.get_records_by_category('').limit(10)
            records.sort('score', -1)
        for r in records:
            print(r)
            records_values.append(r)

        data = []

        for idx, r in enumerate(records_values):
            category = self.conn.get_field_by_id("categories", r['categorycode'], 'name')
            user = self.conn.get_field_by_id("utenti", r['usercode'], 'nome')
            data.append([category[0]['name'], user[0]['nome'], r['score']])
            print(r)
        while len(data) < 10:
            data.append(["-", "-", "-"])

        t = Table(data)
        t.save_fig()
        return t.get_fig()

    def game_stats(self, func, category_name):
        if category_name != '':
            categorycode = self.conn.get_id_by_name("categories", category_name)
            games = self.conn.get_games_stats(categorycode[0]['_id'])
        else:
            games = self.conn.get_games_stats('')

        classic_num = 0
        time_num = 0
        true_false_num = 0
        hangman_num = 0
        classic = 0
        time = 0
        true_false = 0
        hangman = 0
        for g in games:
            if g['gamecode'] == 'cl':
                classic_num = classic_num + g['totalgame']
                classic = classic + g['totalscore']
            elif (g['gamecode'] == 'tm'):
                time_num = time_num + g['totalgame']
                time = time + g['totalscore']
            elif (g['gamecode'] == 'tf'):
                true_false_num = true_false_num + g['totalgame']
                true_false = true_false + g['totalscore']
            elif (g['gamecode'] == 'im'):
                hangman_num = hangman_num + g['totalgame']
                hangman = hangman + g['totalscore']
        games_names = ['Classico', 'A Tempo', 'Vero o Falso', 'Impiccato']

        tipo = ""
        if func == 'played':
            games_values = [classic_num, time_num, true_false_num, hangman_num]
            tipo = 'Partite Giocate'
        if func == 'scores':
            games_values = [classic, time, true_false, hangman]
            tipo = 'Punteggi'
        if func == 'scores_avg':
            if classic_num > 0:
                classic_num = classic / classic_num
            if time_num > 0:
                time_num = time / time_num
            if true_false_num > 0:
                true_false_num = true_false / true_false_num
            if hangman_num > 0:
                hangman_num = hangman / hangman_num
            games_values = [classic_num, time_num, true_false_num, hangman_num]
            tipo = 'Media dei Punteggi'
        h = Histogram(games_names, games_values, tipo, 'Modalità di gioco')
        h.save_fig()
        return h.get_fig()

